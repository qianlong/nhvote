from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    #url(r'^$', 'voteapp.views.index', name='index'),
    # url(r'^voteapp/', include('voteapp.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    url(r'^submit/$', 'vote.views.submit'),
    url(r'^allresults/$', 'vote.views.allresults'),
    url(r'^results/$', 'vote.views.results'),
    url(r'^info/$', 'vote.views.info'),
    url(r'^$', 'vote.views.index'),
    
)
