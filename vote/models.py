from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Position(models.Model):
  title = models.CharField(max_length=50)
  description = models.CharField(max_length=1000)

  def __unicode__(self):
    return self.title
  
class Candidate(models.Model):
  position = models.ForeignKey(Position)
  name = models.CharField(max_length=50)
  platform = models.CharField(max_length=1000)
  votes = models.IntegerField(default=0)
  writein = models.BooleanField(default=False)

  def __unicode__(self):
    return self.name

class Voter(models.Model):
  user = models.OneToOneField(User)
  voted = models.BooleanField(default=False)

class Vote(models.Model):
  voter = models.ForeignKey(User)
  position = models.ForeignKey(Position)
  candidate = models.ForeignKey(Candidate)


