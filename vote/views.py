from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.http import HttpResponse
from django.core.exceptions import ObjectDoesNotExist

# Create your views here.
from django.contrib.auth.models import User
from django.db.models import F
from vote.models import Position
from vote.models import Vote
from vote.models import Voter
from vote.models import Candidate

AUTHORIZED = set(["annayq", "qlong"])
# TODO fix this hackiness
POLLS_OPEN = False
def index(request):
  # check if user already voted
  # redirect to voted.html if voted
  # create voter model if doesn't exist
  # do we need to check if request.user.is_authenticated()?
  # TODO add in poll opening and closing feature (see line 15)
  try:
    voter = Voter.objects.get(user=request.user)
  except ObjectDoesNotExist:
    voter = Voter(user=request.user)
    voter.save()
  if voter.voted:
    user_votes = Vote.objects.filter(voter=request.user)
    return render_to_response('voted.html', { 'user': request.user, 'user_votes': user_votes, 'polls_open': POLLS_OPEN  })

  positions = Position.objects.all()
  return render_to_response('index.html', { 'positions': positions, 'user': request.user, 'voted': voter.voted, 'polls_open': POLLS_OPEN }, context_instance=RequestContext(request)) 

def submit(request):
  # name = position.title, value = candidate.name
  # TODO error checking if someone clicks directly on this link, then there would be nothing in request.POST
  positions = Position.objects.all()
  for position in positions:
    key = position.title
    # if key does not exist, default to abstain, though this shouldn't happen with client side checking
    value = request.POST.get(key, u'Abstain')
    if value != u'Abstain':
      try:
        candidate = position.candidate_set.get(name=value)
      except ObjectDoesNotExist:
        # create new candidate object (this is meant for write-ins)
        candidate = Candidate(position = position, name = value, platform = 'None', writein = True) # TODO: check that value is <= 200 chars, etc.
      #candidate.votes = F('votes') + 1
      candidate.votes = candidate.votes + 1
      candidate.save()
      #candidate.update(votes=F('votes')+1)

      # create a vote
      vote = Vote(voter=request.user, position=position, candidate=candidate)
      vote.save()

  user = User.objects.get(username=request.user)
  voter = Voter.objects.get(user=user)
  voter.voted = True
  voter.save() 

  return redirect('/vote')

def results(request):
  if request.user.username not in AUTHORIZED:
    return render_to_response('not_authorized.html')

  # TODO show total votes and percentages for each position and candidate, maybe on client side
  positions = Position.objects.all()
  return render_to_response('results.html', { 'positions': positions })

def info(request):
  positions = Position.objects.all()
  return render_to_response('info.html', { 'positions': positions })

def allresults(request):
  if request.user.username not in AUTHORIZED:
    return render_to_response('not_authorized.html')

  positions = Position.objects.all() 
  voters = Voter.objects.all()
  # {'name': name, 'votes': [] }
  votes_array = []
  for voter in voters: 
    votes = Vote.objects.filter(voter=voter.user).order_by("position")
    votes_dict = {}
    for vote in votes:
      votes_dict[vote.position.title] = vote.candidate.name 
    array = []
    for position in positions:
      array.append(votes_dict.get(position.title, "None"))
    v_dict = {'name': voter.user.username, 'votes': array}
    votes_array.append(v_dict)

  return render_to_response('allresults.html', {'positions': positions, 'votes_array': votes_array})
