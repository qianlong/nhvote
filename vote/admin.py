from django.contrib import admin
from vote.models import Position
from vote.models import Candidate

# Register your models here.

class CandidateInline(admin.StackedInline):
  model = Candidate
  extra = 1

class PositionAdmin(admin.ModelAdmin):
  fieldsets = [ 
    (None,                {'fields': ['title']}),
    ('Description Info',  {'fields': ['description']}),
  ]
  inlines = [CandidateInline]

admin.site.register(Position, PositionAdmin)

